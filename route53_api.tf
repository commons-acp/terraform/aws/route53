resource "aws_route53_record" "r53_record_api_with_gateway" {
  count = var.api_subdomain.record_api.ip == "" ? 1 :0
  name    = var.api_subdomain.record_api.name
  type    = "A"
  zone_id = var.zone_id

  alias {
    evaluate_target_health = true
    name                   = var.api_gateway_domain_name
    zone_id                = var.api_gateway_zone_id
  }
}
resource "aws_route53_record" "r53_record_api" {
  count = var.api_subdomain.record_api.ip != "" ? 1 :0
  name    = var.api_subdomain.record_api.name
  type    = "A"
  zone_id = var.zone_id
  ttl     = "300"
  records = [var.api_subdomain.record_api.ip]
}