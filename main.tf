

terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws-cloudfront = {
      source  = "hashicorp/aws"
      version = "> 3.70.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "> 3.70.0"
    }
  }

}
