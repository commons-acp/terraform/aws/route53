
variable "aws_region" {
  description = "AWS region"
}

# front
variable "front_subdomain" {
  description = "The front subdomain"
}


#API
variable "api_subdomain" {
    type = map(object({
      name = string
      ip = string
    }))
  default =  { record_api = { name = "", ip = "" } }
}

variable "zone_id" {
  description = "the zone id"
}

variable "api_gateway_zone_id" {
  description = ""
  default = ""
}
variable "api_gateway_domain_name" {
  description = ""
  default = ""
}